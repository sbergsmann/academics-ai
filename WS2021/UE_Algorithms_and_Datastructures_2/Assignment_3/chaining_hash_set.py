from chaining_hash_node import ChainingHashNode


class ChainingHashSet():
    def __init__(self, capacity=0):
        self.hash_table = [None] * capacity
        self.table_size = 0
        self.capacity = capacity

    def get_hash_code(self, key):
        """Hash function that calculates a hash code for a given key using the modulo division.
        :param key:
        Key for which a hash code shall be calculated according to the length of the hash table.
        :return:
        The calculated hash code for the given key.
        """
        return key % self.capacity

    def get_hash_table(self):
        """(Required for testing only)
        :return the hash table.
        """
        return self.hash_table

    def set_hash_table(self, table):
        """(Required for testing only) Set a given hash table..
        :param table: Given hash table which shall be used.

        !!!
        Since this method is needed for testing we decided to implement it.
        You do not need to change or add anything.
        !!!

        """
        self.hash_table = table
        self.capacity = len(table)
        self.table_size = 0
        for node in table:
            while node is not None:
                self.table_size += 1
                node = node.next

    def get_table_size(self):
        """returns the number of stored keys (keys must be unique!)."""
        return self.table_size

    def insert(self, key):
        """Inserts a key and returns True if it was successful. If there is already an entry with the
          same key, the new key will not be inserted and False is returned.
         :param key:
         The key which shall be stored in the hash table.
         :return:
         True if key could be inserted, or False if the key is already in the hash table.
         :raises:
         a ValueError if any of the input parameters is None.
         """
        if key is None:
            raise ValueError("parameter key is None!")

        key_hash_code = self.get_hash_code(key=key)
        current_chain_node = self.hash_table[key_hash_code]

        inserted = False
        while inserted is not True:
            if current_chain_node is None:
                self.hash_table[key_hash_code] = ChainingHashNode(key=key)
                inserted = True
                continue

            if current_chain_node.key == key:
                break

            if current_chain_node.next is None:
                current_chain_node.next = ChainingHashNode(key=key)
                inserted = True
                continue
            else:
                current_chain_node = current_chain_node.next
        self.table_size = self.table_size + 1 if inserted else self.table_size
        return inserted

    def contains(self, key):
        """Searches for a given key in the hash table.
         :param key:
         The key to be searched in the hash table.
         :return:
         True if the key is already stored, otherwise False.
         :raises:
         a ValueError if the key is None.
         """
        if key is None:
            raise ValueError("parameter key is None!")

        key_hash_code = self.get_hash_code(key=key)
        current_chain_node = self.hash_table[key_hash_code]

        found = False
        while found is not True:
            if current_chain_node is None:
                break
            if current_chain_node.key == key:
                found = True
                continue
            else:
                current_chain_node = current_chain_node.next
        return found

    def remove(self, key):
        """Removes the key from the hash table and returns True on success, False otherwise.
        :param key:
        The key to be removed from the hash table.
        :return:
        True if the key was found and removed, False otherwise.
        :raises:
        a ValueError if the key is None.
        """
        if key is None:
            raise ValueError("parameter key is None!")

        key_hash_code = self.get_hash_code(key=key)
        current_chain_node = self.hash_table[key_hash_code]

        removed = False
        if current_chain_node is not None:
            if current_chain_node.key == key:
                self.hash_table[key_hash_code] = current_chain_node.next
                removed = True
        else:
            self.table_size = self.table_size - 1 if removed else self.table_size
            return removed

        while removed is not True:
            if current_chain_node.next is None:
                break

            if current_chain_node.next.key == key:
                current_chain_node.next = current_chain_node.next.next
                removed = True
                continue
            current_chain_node = current_chain_node.next

        self.table_size = self.table_size - 1 if removed else self.table_size
        return removed

    def clear(self):
        """Removes all stored elements from the hash table by setting all nodes to None.
        """
        self.hash_table = [None] * self.capacity
        self.table_size = 0

    def to_string(self):
        """Returns a string representation of the hash table (array indices and stored keys) in the format
            Idx_0 {Node, Node, ... }, Idx_1 {...}
            e.g.: 0 {13}, 1 {82, 92, 12}, 2 {2, 32}, """

        chaining_nodes_placeholder = list()
        for node in self.hash_table:
            chaining_nodes = set()
            
            while node is not None:
                chaining_nodes.add(node.key)
                node = node.next
            chaining_nodes_placeholder.append(chaining_nodes)
            return ", ".join(f"{idx} {cnodes}" for idx, cnodes in zip(range(self.capacity), chaining_nodes_placeholder))
