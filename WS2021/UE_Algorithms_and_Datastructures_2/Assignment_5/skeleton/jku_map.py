from graph import Graph
from step import Step
from vertex import Vertex


class JKUMap(Graph):

    def __init__(self):
        super().__init__()
        v_spar = self.insert_vertex("Spar")
        v_lit = self.insert_vertex("LIT")
        v_porter = self.insert_vertex("Porter")
        v_openlab = self.insert_vertex("Open Lab")
        v_bank = self.insert_vertex("Bank")
        v_chat = self.insert_vertex("Chat")
        v_khg = self.insert_vertex("KHG")
        v_library = self.insert_vertex("Library")
        v_lui = self.insert_vertex("LUI")
        v_parking = self.insert_vertex("Parking")
        v_teichwerk = self.insert_vertex("Teichwerk")
        v_sp1 = self.insert_vertex("SP1")
        v_bellacasa = self.insert_vertex("Bella Casa")
        v_sp3 = self.insert_vertex("SP3")
        v_papaya = self.insert_vertex("Papaya")
        v_castle = self.insert_vertex("Castle")
        v_jkh = self.insert_vertex("JKH")

        self.insert_edge(v_spar, v_lit, 50)
        self.insert_edge(v_porter, v_lit, 80)
        self.insert_edge(v_spar, v_porter, 103)
        self.insert_edge(v_openlab, v_porter, 70)
        self.insert_edge(v_porter, v_bank, 100)
        self.insert_edge(v_khg, v_bank, 150)
        self.insert_edge(v_spar, v_khg, 165)
        self.insert_edge(v_khg, v_parking, 190)
        self.insert_edge(v_parking, v_bellacasa, 145)
        self.insert_edge(v_parking, v_sp1, 240)
        self.insert_edge(v_sp1, v_sp3, 130)
        self.insert_edge(v_sp1, v_lui, 175)
        self.insert_edge(v_teichwerk, v_lui, 135)
        self.insert_edge(v_lui, v_library, 90)
        self.insert_edge(v_chat, v_lui, 240)
        self.insert_edge(v_library, v_chat, 160)
        self.insert_edge(v_chat, v_bank, 115)
        self.insert_edge(v_castle, v_papaya, 85)
        self.insert_edge(v_papaya, v_jkh, 80)

    def get_shortest_path_from_to(self, from_vertex: Vertex, to_vertex: Vertex):
        """
        This method determines the shortest path between two POIs "from_vertex" and "to_vertex".
        It returns the list of intermediate steps of the route that have been found
        using the dijkstra algorithm.

        :param from_vertex: Start vertex
        :param to_vertex:   Destination vertex
        :return:
           The path, with all intermediate steps, returned as an list. This list
           sequentially contains each vertex along the shortest path, together with
           the already covered distance (see example on the assignment sheet).
           Returns None if there is no path between the two given vertices.
        :raises ValueError: If from_vertex or to_vertex is None, or if from_vertex equals to_vertex
        """
        if from_vertex is None or to_vertex is None or from_vertex == to_vertex:
            raise ValueError("Either from_vertex or to_vertex is None or they are the same!")

        # initialize
        distances = dict((vertex.name, float("Inf")) for vertex in self.vertices if vertex is not from_vertex)
        distances[from_vertex.name] = 0
        paths = dict((vertex.name, []) for vertex in self.vertices)
        paths[from_vertex.name] = [Step(point=from_vertex, covered_distance=0)]

        _, paths = self._dijkstra(
            cur=from_vertex,
            visited_list=list(),
            distances=distances,
            paths=paths
        )

        return paths[to_vertex.name] if paths[to_vertex.name] else None

    def get_steps_for_shortest_paths_from(self, from_vertex: Vertex):
        """
        This method determines the amount of "steps" needed on the shortest paths
        from a given "from" vertex to all other vertices.
        The number of steps (or -1 if no path exists) to each vertex is returned
        as a dictionary, using the vertex name as key and number of steps as value.
        E.g., the "from" vertex has a step count of 0 to itself and 1 to all adjacent vertices.

        :param from_vertex: start vertex
        :return:
          A map containing the number of steps (or -1 if no path exists) on the
          shortest path to each vertex, using the vertex name as key and the number of steps as value.
        :raises ValueError: If from_vertex is None.
        """
        if from_vertex is None:
            raise ValueError("from_vertex is None!")

        # initialize
        distances = dict((vertex.name, float("Inf")) for vertex in self.vertices if vertex is not from_vertex)
        distances[from_vertex.name] = 0
        paths = dict((vertex.name, []) for vertex in self.vertices)
        paths[from_vertex.name] = [Step(point=from_vertex, covered_distance=0)]

        _, paths = self._dijkstra(
            cur=from_vertex,
            visited_list=list(),
            distances=distances,
            paths=paths
        )

        steps = dict((item[0], len(item[1]) - 1) for item in paths.items())

        return steps

    def get_shortest_distances_from(self, from_vertex: Vertex):
        """
        This method determines the shortest paths from a given "from" vertex to all other vertices.
        The shortest distance (or -1 if no path exists) to each vertex is returned
        as a dictionary, using the vertex name as key and the distance as value.

        :param from_vertex: Start vertex
        :return
           A dictionary containing the shortest distance (or -1 if no path exists) to each vertex,
           using the vertex name as key and the distance as value.
        :raises ValueError: If from_vertex is None.
        """
        if from_vertex is None:
            raise ValueError("from_vertex is None!")

        # initialize
        distances = dict((vertex.name, float("Inf")) for vertex in self.vertices if vertex is not from_vertex)
        distances[from_vertex.name] = 0
        paths = dict((vertex.name, []) for vertex in self.vertices)
        paths[from_vertex.name] = [Step(point=from_vertex, covered_distance=0)]

        distances, _ = self._dijkstra(
            cur=from_vertex,
            visited_list=list(),
            distances=distances,
            paths=paths
        )

        return distances

    def _dijkstra(self, cur: Vertex, visited_list, distances: dict, paths: dict):
        """
        This method is expected to be called with correctly initialized data structures and recursively calls itself.

        :param cur: Current vertex being processed
        :param visited_list: List which stores already visited vertices.
        :param distances: Dict (nVertices entries) which stores the min. distance to each vertex.
        :param paths: Dict (nVertices entries) which stores the shortest path to each vertex.
        """

        for vertex in self.get_adjacent_vertices(vertex=cur):
            if vertex in visited_list:
                continue
            if distances[cur.name] + self.find_edge(v1=cur, v2=vertex).weight < distances[vertex.name]:
                distances[vertex.name] = distances[cur.name] + self.find_edge(v1=cur, v2=vertex).weight
                paths[vertex.name] = paths[cur.name] + [Step(point=vertex, covered_distance=distances[vertex.name])]

        visited_list.append(cur)
        not_visited_vertices = [vertex for vertex in self.vertices if vertex not in visited_list]
        if not not_visited_vertices:
            for vertex_name in distances.keys():
                if distances[vertex_name] == float("Inf"):
                    distances[vertex_name] = -1
            return distances, paths

        not_visited_vertices_distances = [(vertex.name, distances[vertex.name]) for vertex in not_visited_vertices]
        self._dijkstra(
            cur=self.find_vertex(min(not_visited_vertices_distances, key=lambda item: item[1])[0]),
            visited_list=visited_list,
            distances=distances,
            paths=paths
        )
        return distances, paths


if __name__ == "__main__":
    jku_map = JKUMap()
    result = jku_map.get_steps_for_shortest_paths_from(jku_map.find_vertex("LUI"))
    print(result)
