from avl_node import AVLNode


class AVLTree:

    def __init__(self):
        """Default constructor. Initializes the AVL tree.
        """
        self.root = None

        # tree size to be tracked
        self.size = 0

    def get_tree_root(self):
        """
        Method to get the root node of the AVLTree
        :return AVLNode -- the root node of the AVL tree
        """
        return self.root

    def get_tree_height(self):
        """Retrieves tree height.
        :return -1 in case of empty tree, current tree height otherwise.
        """
        if self.root is None:
            return -1
        else:
            return self.root.height

    def get_tree_size(self):
        """Yields number of key/value pairs in the tree.
        :return Number of key/value pairs.
        """
        return self.size

    def to_array(self):
        """Yields an array representation of the tree's values (pre-order).
        :return Array representation of the tree values.
        """
        return [key for key in self.preorder_traversal(node=self.root)]

    def find_by_key(self, key):
        """Returns value of node with given key.
        :param key: Key to search.
        :return Corresponding value if key was found, None otherwise.
        :raises ValueError if the key is None
        """
        if key is None:
            raise ValueError("Key is None!")

        if not self.root:
            return None
        current_node = self.root
        while current_node.key != key:
            current_node = current_node.left if key < current_node.key else current_node.right
            if not current_node:
                return None
        return current_node.value

    def insert(self, key, value):
        """Inserts a new node into AVL tree.
        :param key: Key of the new node.
        :param value: Data of the new node. May be None. Nodes with the same key
        are not allowed. In this case False is returned. None-Keys and None-Values are
        not allowed. In this case an error is raised.
        :return True if the insert was successful, False otherwise.
        :raises ValueError if the key or value is None.
        """
        if key is None or value is None:
            raise ValueError("Key or value is None!")

        # search for place to insert
        if not self.root:
            self.root = AVLNode(key=key, value=value)
            self.size += 1
            return True

        current_node = self.root
        while True:
            if current_node.key == key:
                return False

            _current_node_children = current_node.left if key < current_node.key else current_node.right
            if _current_node_children is None:
                # found a spot in the tree; insert new node
                new_node = AVLNode(key=key, value=value)
                new_node.parent = current_node
                if key < current_node.key:
                    current_node.left = new_node
                else:
                    current_node.right = new_node
                break
            else:
                current_node = _current_node_children

        self._update_heights(node=new_node)

        # search for imbalances
        self._restructuring(node=new_node)

        self.size += 1
        return True

    def remove_by_key(self, key):
        """Removes node with given key.

        :param key: Key of node to remove.
        :return True If node was found and deleted, False otherwise.
        @raises ValueError if the key is None.
        """
        if key is None:
            raise ValueError("Key is None!")

        # search for node
        if not self.root:
            return False
        remove_node = self.root
        while remove_node.key != key:
            remove_node = remove_node.left if key < remove_node.key else remove_node.right
            if not remove_node:
                # return false if node was not found
                return False

        successor_node = self._search_successor_node_for_removal(node=remove_node)

        # replace dependencies of successor parent node
        if successor_node is not self.root:
            remove_node.value = successor_node.value
            remove_node.key = successor_node.key

            successor_parent_node = successor_node.parent
            successor_parent_node.right = successor_node.right if successor_parent_node.right == successor_node else successor_parent_node.right
            # replace dependencies of successor node

            # update all heights starting from parent node of successor
            self._update_heights(node=successor_parent_node)

            # check for inbalance starting at the successor parent node
            self._restructuring(node=successor_parent_node)
            self.size -= 1
        else:
            self.root = None
            self.size = 0
        return True

    def preorder_traversal(self, node: AVLNode = None):
        """Generator for preorder traversal of a (sub)tree
        
        :param node: the node from where to start traversing
        :return: yield the corresponding node's key according to traversal
        """
        if node:
            yield node.key
            yield from self.preorder_traversal(node.left)
            yield from self.preorder_traversal(node.right)

    def inorder_traversal(self, node: AVLNode = None):
        """Generator for inorder traversal of a (sub)tree

        :param node: the node from where to start traversing
        :return: yield the corresponding node's key according to traversal
        """
        if node:
            yield from self.inorder_traversal(node.left)
            yield node
            yield from self.inorder_traversal(node.right)

    def __restructuring(
            self,
            node: AVLNode = None,
            grandparent_node: AVLNode = None
    ):
        """Restructures the tree based on the passed node and its grandparent node (x, z).

        :param node: node which grandparent is unbalanced
        :param grandparent_node: unbalanced node
        :return: new root of the rebalanced subtree
        """
        x, y, z = node, node.parent, grandparent_node
        a, b, c = None, None, None

        # x, y, z according to inorder traversal
        for node in self.inorder_traversal(node=z):
            if node in [x, y, z]:
                if not a:
                    a = node
                    continue
                elif not b:
                    b = node
                    continue
                else:
                    c = node
                    break

        # gather all subtree root nodes in inorder traversal
        subtree_root_list = list()
        for node in [a, b, c]:
            if node.left not in [a, b, c]:
                subtree_root_list.append(node.left)
            if node.right not in [a, b, c]:
                subtree_root_list.append(node.right)

        # connection node for restructuring
        connection_node = z.parent if z.parent else None
        # restructuring
        if connection_node:
            if connection_node.right == z:
                connection_node.right = b
            else:
                connection_node.left = b
            b.parent = connection_node
        else:
            # if the parent of the grandparent is none then z is the root
            self.root = b
            b.parent = None

        b.left = a
        b.right = c

        a.left = subtree_root_list[0]
        a.right = subtree_root_list[1]
        for subtree_root in subtree_root_list[:2]:
            if subtree_root:
                subtree_root.parent = a
        a.parent = b
        a.height = max(
            subtree_root_list[0].height if subtree_root_list[0] else -1,
            subtree_root_list[1].height if subtree_root_list[1] else -1) + 1

        c.left = subtree_root_list[2]
        c.right = subtree_root_list[3]
        for subtree_root in subtree_root_list[2:]:
            if subtree_root:
                subtree_root.parent = c
        c.parent = b
        c.height = max(
            subtree_root_list[2].height if subtree_root_list[2] else -1,
            subtree_root_list[3].height if subtree_root_list[3] else -1) + 1

        self._update_heights(node=b)

        return b

    def _restructuring(self, node: AVLNode = None):
        """Checks the validity and structure of the nodes to be checked for restructuring
        before passing the nodes on to __restructuring.

        :param node: The start node from where balance checks upwards should start.
        """
        while node is not None:
            if not self._is_balanced(node=node):
                # child_node may be none if current_node is a leaf
                child_node = node.right if \
                    (node.right.height if node.right else -1) > \
                    (node.left.height if node.left else -1) \
                    else node.left

                # if child_node:
                # grandchild_node may be none if child_node is a leaf
                grandchild_node = child_node.right if \
                    (child_node.right.height if child_node.right else -1) > \
                    (child_node.left.height if child_node.left else -1) \
                    else child_node.left

                # if grandchild_node:
                node = self.__restructuring(
                    node=grandchild_node,
                    grandparent_node=node
                )
                continue
            node = node.parent

    def _update_heights(self, node: AVLNode = None):
        """Recursively update heights of all parents upwards.

        :param node: Node from which on the tree height should be adjusted.
        """
        height_right = node.right.height if node.right else -1
        height_left = node.left.height if node.left else -1
        node.height = max(height_right, height_left) + 1
        if node == self.root:
            return None
        self._update_heights(node=node.parent)

    def _search_successor_node_for_removal(self, node: AVLNode = None):
        """Algorithm that gives according to current structure the suitable successor for a node
        in inorder-traversal.

        :param node: node for which a successor node is needed.
        :return: the successor node.
        """
        # case 1: no right and no left tree
        if not node.right and not node.left:
            # we do not need an successor, just remove the node
            return node
        # case 2: no right tree
        elif not node.right and node.left:
            # reassign and recrusively search for a successor node in the left subtree
            self._search_successor_node_for_removal(node=node.left)
        # case 3: a right subtree
        else:
            for _node in self.inorder_traversal(node=node.right):
                return _node
        return node

    @staticmethod
    def _is_balanced(node: AVLNode = None):
        """Checks whether a passed node is balanced according to avl tree constraints.

        :param node: node to be checked for imbalance.
        :return: True if node is balanced, else False.
        """
        if abs(
                (node.right.height if node.right else -1) - (node.left.height if node.left else -1)
        ) >= 2:
            return False
        return True
