import hedgehog as hh
import pandas as pd

SAVE = False


def build_network():
    bn = hh.BayesNet(
        ("Relaxes", "Sings"),
        ("Whistles", "Sings"),
        ("Sings", "Colorful"),
        ("Vacation", "Happy"),
        ("Colorful", "Happy")
    )
    return bn


def initialize_probabilities(bn):
    bn.P["Relaxes"] = pd.Series({True: .35, False: .65})
    bn.P["Whistles"] = pd.Series({True: .2, False: .8})
    bn.P["Sings"] = pd.Series({
        (False, False, True): .4,
        (False, False, False): .6,
        (False, True, True): .8,
        (False, True, False): .2,
        (True, False, True): .05,
        (True, False, False): .95,
        (True, True, True): .7,
        (True, True, False): .3
    })
    bn.P["Colorful"] = pd.Series({
        (False, True): .4,
        (False, False): .6,
        (True, True): .6,
        (True, False): .4
    })
    bn.P["Vacation"] = pd.Series({True: .02, False: .98})
    bn.P["Happy"] = pd.Series({
        (False, False, True): .4,
        (False, False, False): .6,
        (True, False, True): .8,
        (True, False, False): .2,
        (False, True, True): .05,
        (False, True, False): .95,
        (True, True, True): .7,
        (True, True, False): .3
    })
    return bn


if __name__ == '__main__':
    bn = build_network()
    # optional: visualize network to check whether the structure is correct#
    if SAVE:
        dot = bn.graphviz()
        dot.attr(label=r"\nUnicorn Bayesian Network\nSeverin Bergsmann\nk12008683")
        path = dot.render("Ass5_ExactInterference", directory="figures", format="png", cleanup=True)

    bn = initialize_probabilities(bn)
    bn.prepare()  # implemented in hedgehog, don't remove
    # print(bn.P)

    # compute intermediate probabilities, alpha and exact probabilities
    observation_true = {"Colorful": True, "Whistles": False, "Sings": True, "Vacation": True, "Happy": True}
    colorful_true = bn.predict_proba(observation_true)
    observation_false = {"Colorful": False, "Whistles": False, "Sings": True, "Vacation": True, "Happy": True}
    colorful_false = bn.predict_proba(observation_false)
    alpha = 1 / (colorful_true + colorful_false)

    # print results
    print("P(c, not_w, s, v, h):     ", round(colorful_true, 8))
    print("P(not_c, not_w, s, v, h): ", round(colorful_false, 8))
    print("P(c | not_w, s, v, h):    ", round(colorful_true * alpha, 3))
    print("P(not_c | not_w, s, v, h):", round(colorful_false * alpha, 3))
    print("ALPHA:", round(alpha, 3))
