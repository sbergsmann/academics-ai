import hedgehog as hh

if __name__ == "__main__":
    bn = hh.BayesNet(
        "Colorful",
        ("Thursday", "Party"),
        ("Local Band", "Party"),
        ("Exam", "Party"),
        ("June", "Exam"),
        ("January", "Exam")
    )

    dot = bn.graphviz()
    dot.attr(label=r"\nBayesian Network\nSeverin Bergsmann\nk12008683")
    path = dot.render("Ass5_BayesianNetwork", directory="figures", format="png", cleanup=True)
