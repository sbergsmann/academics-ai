import numpy as np

from ..environment import Environment, Outcome


def eps_greedy(rng, qs, epsilon):
    # this function makes an epsilon greedy decision
    if rng.uniform(0, 1) < epsilon:
        # - with probability p == epsilon, an action is
        # chosen uniformly at random
        return rng.randint(0, 4)
    else:
        # - with probability p == 1 - epsilon, the action
        #   having the currently largest q-value estimate is chosen
        return max([(action, qvalue) for action, qvalue in qs.items()], key=lambda item: item[1])[0]


class QLearning():
    def train(self, env: Environment):
        ########################################
        # please leave untouched
        rng = np.random.RandomState(1234)
        alpha = 0.2
        epsilon = 0.3
        gamma = env.get_gamma()
        n_episodes = 10000
        ########################################

        ########################################
        # initialize the 'table'
        Q = dict()
        for s in range(env.get_n_states()):
            Q[s] = dict()
            for a in range(env.get_n_actions()):
                Q[s][a] = 0.
        ########################################

        for episode in range(1, n_episodes + 1):
            state = env.reset()
            done = False
            while not done:
                action = eps_greedy(rng, Q[state], epsilon)
                next_state, reward, done = env.step(action)

                # update reward
                Q[state][action] = Q[state][action] + alpha * (
                        reward + gamma * max(value for _, value in Q[next_state].items())
                        - Q[state][action])
                state = next_state
        ########################################
        # this computes a deterministic policy
        # from the Q value function
        # along the way, we compute V, the
        # state value function as well
        policy = dict()
        V = dict()
        for s, qs in Q.items():
            policy[s] = dict()
            V[s] = 0.
            best_a = None
            best_q = float('-Inf')
            for a, q in qs.items():
                if q > best_q:
                    best_q = q
                    best_a = a

            # how good is it to be in state 's'?
            # if we take the best action, we can expect to get 'best_q'
            # future reward. hence, being in state V[s] we can expect
            # the same amount of reward ...
            V[s] = best_q
            for a in qs.keys():
                if a == best_a:
                    policy[s][a] = 1.
                else:
                    policy[s][a] = 0.
        ########################################

        return Outcome(n_episodes, policy, V=V, Q=Q)
