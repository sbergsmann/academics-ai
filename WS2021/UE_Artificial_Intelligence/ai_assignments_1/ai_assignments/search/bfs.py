import random

from ..datastructures.queue import Queue
from ..problem import Problem


# please ignore this
def get_solver_mapping():
    return dict(bfs=BFS)


class BFS(object):
    def solve(self, problem: Problem):
        random.seed(1234)
        current_node = problem.get_start_node()

        visited_nodes = set()
        fringe = Queue()

        # counter = 1

        while not problem.is_end(node=current_node):

            visited_nodes.add(current_node)

            successor_nodes = problem.successors(node=current_node)

            """FOR DEBUGGING =========================================================================
            print("run", counter, "fringe", len(fringe), "visited", len(visited_nodes), "successors",
                  len(successor_nodes))
            counter += 1"""

            for successor_node in successor_nodes:
                if successor_node not in visited_nodes:
                    fringe.put(successor_node)

            current_node = fringe.get()
            while True:
                if current_node in visited_nodes:
                    current_node = fringe.get()
                else:
                    break

        return current_node
