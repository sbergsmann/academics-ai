import random

from ..datastructures.priority_queue import PriorityQueue
from ..problem import Problem


def get_solver_mapping():
    return dict(ucs=UCS)


class UCS(object):
    def solve(self, problem: Problem):
        random.seed(1234)

        priority_queue = PriorityQueue()
        visited_nodes = set()

        # counter = 1

        current_node = problem.get_start_node()
        while not problem.is_end(node=current_node):
            visited_nodes.add(current_node)

            successor_nodes = problem.successors(node=current_node)
            for successor_node in successor_nodes:
                if successor_node not in visited_nodes:
                    priority_queue.put(
                        priority=successor_node.cost,
                        value=successor_node
                    )

            """FOR DEBUGGING =========================================================================
            print("run", counter, "fringe", len(priority_queue), "visited", len(visited_nodes), "successors",
                  len(successor_nodes))
            counter += 1"""

            while current_node in visited_nodes:
                current_node = priority_queue.get()

        return current_node
