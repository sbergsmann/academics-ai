import math

from ..datastructures.priority_queue import PriorityQueue
from ..problem import Problem


# please ignore this
def get_solver_mapping():
    return dict(
        astar_ec=ASTAR_Euclidean,
        astar_mh=ASTAR_Manhattan
    )


class ASTAR(object):
    def heuristic(self, current, goal) -> float:
        pass

    def solve(self, problem: Problem):
        priority_queue = PriorityQueue()
        visited_nodes = set()

        current_node = problem.get_start_node()

        while not problem.is_end(node=current_node):
            successors = problem.successors(node=current_node)
            visited_nodes.add(current_node)

            for successor in successors:
                if successor not in visited_nodes:
                    priority_queue.put(
                        priority=self.heuristic(
                            current=successor,
                            goal=problem.get_end_node()
                        ) + successor.cost,
                        value=successor
                    )

            while current_node in visited_nodes:
                current_node = priority_queue.get()

        return current_node


# this is the ASTAR variant with the euclidean distance as a heuristic
# it is registered as a solver with the name 'gbfs_ec'

# please note that in an ideal world, this heuristic should actually be part
# of the problem definition, as it assumes domain knowledge about the structure
# of the problem, and defines a distance to the goal state
class ASTAR_Euclidean(ASTAR):
    def heuristic(self, current, goal):
        cy, cx = current.state
        gy, gx = goal.state
        return math.sqrt((cy - gy) ** 2 + (cx - gx) ** 2)


# this is the ASTAR variant with the manhattan distance as a heuristic
# it is registered as a solver with the name 'gbfs_mh'

# please note that in an ideal world, this heuristic should actually be part
# of the problem definition, as it assumes domain knowledge about the structure
# of the problem, and defines a distance to the goal state
class ASTAR_Manhattan(ASTAR):
    def heuristic(self, current, goal):
        cy, cx = current.state
        gy, gx = goal.state
        return math.fabs((cy - gy)) + math.fabs(cx - gx)
