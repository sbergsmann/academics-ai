# academics-ai

Shared repository to enable enable distance collaboration and education for the bachelor's program for Artificial
Intelligence.

---

### Disclaimer

This repository is intended to motivate and inspire the collaborators to:

- be open-minded and get insights into how other poeple code
- share ideas and concepts on how to solve tasks differently
- enable distance coding collaboration

This repository is **not** intended to be used to:

- copy the published scripts as-is
- let others do the work and benefit from it

Everyone who joins and collaborates within this compound does this on his or her own responsibility and commits to only
use the provided information to solve the tasks better **on his or her own**.

---

## Contribution Guide

last updated: 2021-10-12

### Setup

1. Knowledge Prerequisites:
    - git
    - python
    - poetry
    - cmd
2. Install pip:
    1. download the script from _https://bootstrap.pypa.io/get-pip.py_, cd to the folder and run _python get-pip.py_.
    2. run _python -m pip install --upgrade pip_ to update pip (if needed).
3. Install git (this is win-64x version) via _https://git-scm.com/download/win_.
4. Install [poetry](https://python-poetry.org/):
    1. download the script from _https://install.python-poetry.org_, cd to the folder and run _python install-poetry.py_
       .
    2. run _poetry self update_ to update poetry (if needed).
5. **(Optional)** Install [pyenv](https://github.com/pyenv-win/pyenv-win#installation) for convenient python-version
   control using _pip install pyenv-win --target $HOME\\.pyenv_ and follow the remaining instrutions.
6. **Clone** the gitlab repository using _git clone https://gitlab.com/sbergsmann/academics-ai.git_
7. navigate to the folder _./academics-ai_ and install the dependencies:
    - **(Optional)** _pyenv install PYTHONVERSION_, where PYTHONVERSION is the version number stated in the
      pyproject-toml file (e.g. 3.9.0).
    - _poetry install_
    - or look up the _pyproject.toml_ file install all the dependencies manually via pip

   (support for other dependency management systems may come up)
9. if you use **Pycharm IDE**:
    - mark both _reformat code_ and _optimize imports_ in _File/Settings/Tools/Actions_on_Save_
10. Install the environment with all the necessary dependencies on a **Jupyter notebook kernel** with _ipython kernel
    install --name "academics-ai" --user_
    - more information [here](https://queirozf.com/entries/jupyter-kernels-how-to-add-change-remove#remove-kernel)
    - to **list** all installed kernels use _jupyter kernelspec list_
    - **delete** an existing kernel using _jupyter kernelspec remove academics-ai_

    If you update the poetry enviroment, be aware that you have to **reinstall** the kernel to adapt the changes.

### Assignments

If you are declared to do an assignment, please stick to the following guideline while working:

- always update your local main branch to be up-to-date
- create a new branch that clearly indicates your work (ideally in the manner **
  YYYY-MM-DD_Subject-with-any-assignment-number**)
    - look up previous branches to get an idea
- create a course-folder in the respective semester directory (e.g. WS2021/**UE_Artificial_Intelligence**)
- create a sub-folder (if necessary in case of multiple files per project) to work in (e.g.
  WS2021/UE_Artificial_Intelligence/**Assignment_1**)
    - stick to naming conventions if there are already project folders
- finish your work and **structure your commits** before you push to the repo
- create a pull request and add the respective people who are assigned in this course as reviewers
- look over the proposed changes and update your project before you merge your branch into main

### Some side notes

- Please be aware when using existing and updated material on the repo of the plagiarism scans! The aim is still to do
  the projects on your own!
- If any sensitive personal information is needed in the code, be sure to not hardcode the information into the code!
  Stay general and use [dotenv](https://pypi.org/project/python-dotenv/) or [keyring](https://pypi.org/project/keyring/)
  to maintain environment variables. (if a _.env_ file is created, be sure to add the file to a _.gitignore_ file)
- If you are reviewing code, be nice and have in mind that we are all learning ;)
- Have fun and be open-minded to new ways and styles of programming :)
- In case of any problems or concerns just get in contact (ideally **create an issue** on the repo itself)
